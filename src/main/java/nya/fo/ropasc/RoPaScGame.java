package nya.fo.ropasc;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by 0da on 10.12.2023 17:07; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 *
 * <br>
 * Реализация игры "камень, ножницы, бумага" для игры через {@code telnet}. Реализация намерено сделана для работы в одном потоке для упрощения кода.
 */
public class RoPaScGame extends Thread {

    /**
     * Варианты ответа в игре. Вынесено в отдельный класс во избежания опечаток.
     */
    enum Variant {
        ROCK {
            // Камень побеждает ножницы.
            @Override boolean betterThan(Variant other) {
                return other == SCISSORS;
            }
        },

        PAPER {
            // Бумага побеждает камень.
            @Override boolean betterThan(Variant other) {
                return other == ROCK;
            }
        },

        SCISSORS {
            // Ножницы побеждают бумагу.
            @Override boolean betterThan(Variant other) {
                return other == PAPER;
            }
        };

        /**
         * Все варианты в виде одной строки разделенные запятой с пробелом.
         */
        public final static String ALL_AS_STRING = Stream.of(values())
                .map(Objects::toString)
                .map(String::toLowerCase)
                .collect(Collectors.joining(", "));

        /**
         * Метод определяет, является ли текущий вариант лучше, чем другой.
         * {@snippet :
         *     if (ROCK.betterThan(SCISSORS)) {
         *        System.out.println("always true");
         *     }
         *}
         *
         * @param other вариант который нужно проверить.
         * @return {@code true} в случае если {@code other} проигрывает этому варианту.
         */
        abstract boolean betterThan(Variant other);
    }

    /**
     * Класс для хранения состояния игрока.
     */
    private class PlayerContext {
        /**
         * Имя игрока.
         */
        String name;

        /**
         * Строка для конкатенации ввода игрока.
         * <br>
         * {@code telnet} отправляет сообщение как попало, одни реализации отправляют всю строку при нажатии {@code enter},
         * другие реализации {@code telnet} отправляют по одному символу сразу как только клавиша была нажата, также в случае первых строка может
         * быть разбита на несколько кусков т.к. целиком не помещалась в какие-то заданные рамки пакета.
         * Это поле аккумулирует ввод игрока до тех пор, пока не придет символ {@link #SEPARATOR}.
         */
        StringBuilder input = new StringBuilder();

        /**
         * Ответ игрока в игре, по умолчанию {@code null}.
         */
        Variant move;

        /**
         * Ссылка на оппонента этого игрока.
         */
        PlayerContext opponent;

        /**
         * Ссылка на {@link SocketChannel} что ассоциирован с этим игроком.
         */
        final WeakReference<SocketChannel> channel;

        public PlayerContext(SocketChannel channel) {
            this.channel = new WeakReference<>(channel);
        }

        /**
         * Метод закрывает канал пользователя в случае если он есть и еще открыт. Закрытие канала сопровождается сообщением пользователю.
         */
        public void close() {
            sendMessage(this, "Bye, bye.");
            Optional.ofNullable(channel.get())
                    .filter(SocketChannel::isOpen)
                    .ifPresent(socketChannel -> {
                        try {
                            socketChannel.close();
                        } catch (IOException e) {/* Channel was closed. */}
                    });
        }
    }

    /**
     * Лог этого класса.
     */
    private static final Logger log = Logger.getLogger(RoPaScGame.class.getName());

    /**
     * Сепаратор по которому определяются разные сообщения пользователя.
     *
     * @see PlayerContext#input
     */
    private static final String SEPARATOR = "\n";

    /**
     * Пустой буфер для проверки открытости канала.
     */
    private static final ByteBuffer EMPTY_BYTE_BUFFER = ByteBuffer.allocate(1);

    /**
     * Порт на котором будет запушен сервер.
     */
    private static final int PORT = 23;

    /**
     * Кодировка строк в которой будут приниматься и отправляться строки.
     */
    private static final Charset CHARSET = StandardCharsets.UTF_8;

    /**
     * Буфер размером 32 байта для считывания строк из сокета.
     */
    private final ByteBuffer readBuffer = ByteBuffer.allocate(32);

    /**
     * Референс на последнего человека в очереди.
     * <br>
     * Т.к. подбор соперника не предполагает ожидание соперника с какой-то подходящей
     * метрикой на вроде рейтинга, нет смысла в очереди реализованной через коллекцию.
     */
    private PlayerContext queuedPlayer;

    public static void main(String[] args) {

        // Запускаем игру в своем потоке и останавливаем основной поток.

        RoPaScGame app = new RoPaScGame();
        app.setName("RoPaSc Game Thread.");
        app.setDaemon(true);

        app.start();

        try {
            log.info("Press <enter> for app termination.");
            //noinspection ResultOfMethodCallIgnored
            System.in.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Метод инициализирует сокет сервера и крутит основной игровой цикл.
     */
    @Override
    public void run() {
        try (var selector = Selector.open();
             var server = ServerSocketChannel.open()) {

            server.bind(new InetSocketAddress(PORT));
            server.configureBlocking(false);
            server.register(selector, SelectionKey.OP_ACCEPT);

            log.info("Server started on port " + PORT);

            while (selector.isOpen() || !Thread.currentThread().isInterrupted()) {
                selector.select(TimeUnit.SECONDS.toMillis(5));

                // Основная работа с сокетами.
                // 1. Если это основной сокет сервера то принимаем новое соединение.
                // 2. Если это сокет пользователя то оперируем с вводом пользователя.
                var keyIterator = selector.selectedKeys().iterator();
                while (keyIterator.hasNext()) {
                    var key = keyIterator.next();
                    keyIterator.remove();

                    //noinspection resource
                    switch (key.channel()) {
                        // При подключении нового пользователя ассоциируем с ним новый контекст игрока и подписываемся на ивент ввода.
                        case ServerSocketChannel s when key.isAcceptable() -> {
                            try {
                                var socket = s.accept();

                                if (socket != null) {

                                    var context = new PlayerContext(socket);

                                    socket.configureBlocking(false);
                                    socket.register(selector, SelectionKey.OP_READ, context);

                                    sendMessage(context, "Enter your nickname: ");
                                }

                            } catch (IOException e) {/* Channel was closed. */}
                        }

                        // Получаем контекст игрока, на практике он не может отсутствовать, т.к. при каждом подключении игроку выдается новый контекст.
                        case SocketChannel channel when key.isReadable() && key.attachment() instanceof PlayerContext context -> {

                            StringBuilder builder = context.input;

                            // Считываем из сокета контент сколько сможем.
                            try {
                                readBuffer.clear();
                                while (channel.read(readBuffer) > 0) {
                                    readBuffer.flip();
                                    builder.append(CHARSET.decode(readBuffer));
                                    readBuffer.clear();
                                }
                            } catch (IOException e) {/* Channel was closed. */}

                            // Проверяем не появилось ли сепараторов сигнализирующих о конце сообщения и обрабатываем их по необходимости.
                            for (int i = builder.indexOf(SEPARATOR); i >= 0; i = builder.indexOf(SEPARATOR)) {

                                handlePlayerInput(context, builder.substring(0, i));

                                // Удаляем обработанную часть ввода вместе с сепаратором
                                builder.delete(0, i + SEPARATOR.length() + 1);
                            }
                        }
                        case null, default -> {}
                    }
                }

                // Проходимся по всем сокетам к которым привязан контекст игрока и находим отвалившихся противников.
                for (var key : selector.keys()) {

                    // noinspection resource
                    if (key.attachment() instanceof PlayerContext me && key.channel() instanceof SocketChannel channel) {
                        // Форсированная проверка на факт того что сокет еще открыт, в случае если он открыт то write должен пройти без ошибок.
                        try {
                            EMPTY_BYTE_BUFFER.clear();
                            channel.write(EMPTY_BYTE_BUFFER);
                        } catch (IOException e) {
                            try {
                                channel.close();
                            } catch (IOException ignored) {}
                            if (queuedPlayer == me) {
                                queuedPlayer = null;
                            }
                            continue;
                        }

                        // Если мой оппонент уже определен, то нужно проверить жив ли он.
                        if (me.opponent != null) {
                            // Проверка на одно из следующих условий.
                            // 1. Сокет оппонента был собран GC.
                            // 2. Сокет оппонента не открыт.
                            // Иначе говоря канал противника все еще существует и открыт.
                            boolean opponentIsGood = Optional.of(me.opponent)
                                    .map(o -> o.channel.get())
                                    .filter(SocketChannel::isOpen)
                                    .isPresent();

                            if (!opponentIsGood) {
                                sendMessage(me, "Your opponent escaped.");
                                me.close();
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            log.warning(e.getMessage());
        }

    }

    /**
     * Основной метод для работы с вводом пользователя, все в одном методе для упрощения кода.
     *
     * @param me      пользователь для которого нужно обработать ввод.
     * @param message ввод пользователя.
     */
    private void handlePlayerInput(PlayerContext me, String message) {

        // Убираем эти символы для упрощения работы с пользовательским сообщением и чтоб не сломать ему терминал когда отправляем сообщения с именем обратно.
        message = message.replaceAll("[\r\n]+", "").trim();

        // В случае если строка пуста то ничего не делаем.
        if (message.isEmpty()) {
            return;
        }

        // В случае если у игрока еще нет имени то устанавливаем имя и помещаем его в 'очередь'
        if (me.name == null) {
            me.name = message;

            sendMessage(me, "Hi " + me.name + ". Entering the Q. Wait for opponent.");

            // Если сейчас никого нет в очереди, то помещаем текущего игрока в очередь.
            // Иначе забираем игрока из очереди и начинаем игру.
            if (queuedPlayer == null) {
                queuedPlayer = me;
            } else {
                var opponent = queuedPlayer;
                queuedPlayer = null;

                me.opponent = opponent;
                opponent.opponent = me;

                sendMessage(opponent, "Match found! Your opponent is " + me.name + ". Enter your move (" + Variant.ALL_AS_STRING + "): ");
                sendMessage(me, "Match found! Your opponent is " + opponent.name + ". Enter your move (" + Variant.ALL_AS_STRING + "): ");
            }
            return;
        }

        // Если еще не найден противник. Игнорируем ввод.
        if (me.opponent == null) {
            sendMessage(me, "Wait for opponent.");
            return;
        }

        // Если противник найден. Забираем ответ, последующие команды игнорируем.
        // В случае если оба ввели ответ, то определяем победителя.
        if (me.move == null) {

            // Пытаемся распознать
            try {
                me.move = Variant.valueOf(message.toUpperCase(Locale.ROOT));
                sendMessage(me, "You chose " + me.move + ".");
            } catch (IllegalArgumentException ignored) {
                sendMessage(me, "Incorrect variant, choose one of this: " + Variant.ALL_AS_STRING);
            }

            // Если оппонент тоже уже сделал ход, то решаем кто победил.
            if (me.move != null && me.opponent.move != null) {
                var opponent = me.opponent;

                // Если ответы одинаковы, то играют повторно. Иначе объявляется победа одного из игроков.
                if (me.move == opponent.move) {
                    final var m = "It's a draw! Play again. Enter your move (" + Variant.ALL_AS_STRING + "): ";

                    sendMessage(me, m);
                    sendMessage(opponent, m);
                    me.move = opponent.move = null;
                } else {
                    final var m = me.move.betterThan(opponent.move)
                            ? me.name + " wins with " + me.move + " vs " + opponent.name + " " + opponent.move + "."
                            : opponent.name + " wins with " + opponent.move + " vs " + me.name + " " + me.move + ".";


                    sendMessage(me, m);
                    sendMessage(opponent, m);

                    me.close();
                    opponent.close();
                }
            }

            return;
        }

        sendMessage(me, "Input is not required.");

    }

    /**
     * Метод отправляет сообщение пользователю с постфиксом {@code \r\n}.
     *
     * @param player  пользователь которому нужно отправить сообщение.
     * @param message сообщение которое нужно отправить.
     */
    private void sendMessage(PlayerContext player, String message) {

        var channel = player.channel;
        if (channel == null) return;

        var socketChannel = channel.get();
        if (socketChannel == null) return;

        if (!socketChannel.isOpen()) return;

        try {
            socketChannel.write(CHARSET.encode(CharBuffer.wrap(message + "\r\n")));
        } catch (IOException ignored) {/* Channel was closed. */}
    }

}


