package nya.fo.ropasc;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by 0da on 10.12.2023 21:13; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
class RoPaScGameTest {
    @Test
    void test_variants_1() {
        assertTrue(RoPaScGame.Variant.ROCK.betterThan(RoPaScGame.Variant.SCISSORS));
    }

    @Test
    void test_variants_2() {
        assertTrue(RoPaScGame.Variant.PAPER.betterThan(RoPaScGame.Variant.ROCK));
    }

    @Test
    void test_variants_3() {
        assertTrue(RoPaScGame.Variant.SCISSORS.betterThan(RoPaScGame.Variant.PAPER));
    }

    @Test
    void test_variants_4() {
        assertFalse(RoPaScGame.Variant.ROCK.betterThan(RoPaScGame.Variant.PAPER));
    }

    @Test
    void test_variants_5() {
        assertFalse(RoPaScGame.Variant.PAPER.betterThan(RoPaScGame.Variant.SCISSORS));
    }

    @Test
    void test_variants_6() {
        assertFalse(RoPaScGame.Variant.SCISSORS.betterThan(RoPaScGame.Variant.ROCK));
    }
}