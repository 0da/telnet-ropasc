FROM maven:3.9.5-eclipse-temurin-21-alpine as build

COPY pom.xml .
COPY src src

RUN mvn -B compile

FROM eclipse-temurin:21-jre-alpine

COPY --from=build target/classes classes

ENTRYPOINT ["java","-cp", "/classes/", "nya.fo.ropasc.RoPaScGame"]